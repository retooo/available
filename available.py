import os
import re
import sys
from datetime import date


def usage(error=None):
    if error:
        print(f"ERROR: {error}")

    print("usage: YYYY-MM-DD")
    sys.exit(99)



if len(sys.argv) != 2:
    usage()
target_str = sys.argv[1]


m = re.compile(r'^(\d\d\d\d)-(\d\d)-(\d\d)$').match(target_str.strip())
if not m:
    usage()

year = int(m.group(1))
month = int(m.group(2))
day = int(m.group(3))
try:
    target = date(year, month, day)
except ValueError as e:
    usage(str(e))
    raise(e)

reference = date(2022, 5, 2)

diff = target - reference
available = diff.days % 2 == 1
print("yup" if available else "nope")

