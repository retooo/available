FROM python:3-alpine

WORKDIR /app
COPY available.py /app/

ENTRYPOINT ["python", "available.py"]